﻿using System;
using cAlgo.API;
using cAlgo.API.Internals;
using cAlgo.API.Indicators;
using cAlgo.Indicators;

namespace cAlgo
{
    [Indicator(IsOverlay = true, TimeZone = TimeZones.UTC, AccessRights = AccessRights.None)]
    public class TheGridIndicator : Indicator
    {
        [Parameter(DefaultValue = 5, MinValue = 5)]
        public int Period { get; set; }

        [Parameter(DefaultValue = 10, MinValue = 1)]
        public int MinSizeChannel { get; set; }

        [Parameter(DefaultValue = 12, MinValue = 5)]
        public int MaxSizeChannel { get; set; }

        [Parameter(DefaultValue = 7, MinValue = 5)]
        public int QtdChannel { get; set; }

        [Output("Up Fractal", LineColor = "#e74c3c", PlotType = PlotType.Points, Thickness = 5)]
        public IndicatorDataSeries UpFractal { get; set; }

        [Output("Down Fractal", LineColor = "#3498db", PlotType = PlotType.Points, Thickness = 5)]
        public IndicatorDataSeries DownFractal { get; set; }

        [Output("Channel", LineColor = "#8e44ad", PlotType = PlotType.Points, Thickness = 5)]
        public IndicatorDataSeries ChannelPoint { get; set; }

        public double currentChannel;

        public bool isCanal;

        public enum ModeType
        {
            MODE_UPPER,
            MODE_LOWER
        }

        protected override void Initialize()
        {

            int currentBars = Bars - 1;
            int highCandle = 0;
            isCanal = false;
            bool isFractal = false;

            for (int currentBar = currentBars; currentBar > 1; currentBar--)
            {
                int resistence = iFractal(currentBar, ModeType.MODE_UPPER);

                if (resistence > 0 && isFractal == false)
                {

                    //UpFractal[resistence] = MarketSeries.High[resistence];
                    currentChannel = MarketSeries.High[resistence];
                    highCandle = currentBar;
                    isFractal = true;
                }

                int support = iFractal(currentBar, ModeType.MODE_LOWER);

                if (support > 0 && isFractal == true)
                {
                    isFractal = false;
                    //DownFractal[support] = MarketSeries.Low[support];
                    currentChannel = Math.Abs((currentChannel - MarketSeries.Low[support]) / Symbol.PipValue);

                    if (currentChannel >= MinSizeChannel && currentChannel <= MaxSizeChannel && isCanal == false)
                    {
                        isCanal = true;
                        Print("TAMANHO DO CANAL ATUAL ->", currentChannel);

                        drawChartLine("resistence", MarketSeries.High[highCandle], Color.Red, 2);
                        drawChartLine("support", MarketSeries.Low[support], Color.Blue, 2);

                        drawChannelsUpper("TP/SL_TOP_", MarketSeries.High[highCandle]);
                        drawChannelsLower("TP/SL_BOTTOM_", MarketSeries.Low[support]);

                        ChannelPoint[highCandle] = MarketSeries.High[highCandle];
                        ChannelPoint[support] = MarketSeries.Low[support];

                    }
                }
            }

        }

        private int iFractal(int index, ModeType modeType)
        {
            int period = Period % 2 == 0 ? Period - 1 : Period;
            int middleIndex = index - period / 2;
            double middleValue = modeType == ModeType.MODE_UPPER ? MarketSeries.High[middleIndex] : MarketSeries.Low[middleIndex];

            bool down = true;
            bool up = true;

            if (modeType == ModeType.MODE_UPPER)
            {
                for (int i = 0; i < period; i++)
                {
                    if (middleValue < MarketSeries.High[index - i])
                    {
                        up = false;
                        break;
                    }
                }
                if (up)
                {
                    return middleIndex;
                }
                else
                {
                    return 0;
                }

            }

            if (modeType == ModeType.MODE_LOWER)
            {
                for (int i = 0; i < period; i++)
                {
                    if (middleValue > MarketSeries.Low[index - i])
                    {
                        down = false;
                        break;
                    }
                }
                if (down)
                {
                    return middleIndex;
                }
                else
                {
                    return 0;
                }
            }

            return 0;

        }

        private void drawChartLine(string name, double value, Color color, int thickness)
        {
            Chart.DrawHorizontalLine(name, value, color, thickness);
        }

        private int Bars
        {
            get { return MarketSeries.Close.Count; }
        }

        private void drawChannelsUpper(string name, double price)
        {
            for (int i = 0; i < QtdChannel; i++)
            {
                price = price + (currentChannel * Symbol.PipValue);
                drawChartLine(name + i, price, Color.Green, 2);
            }
        }

        private void drawChannelsLower(string name, double price)
        {
            for (int i = 0; i < QtdChannel; i++)
            {
                price = price - (currentChannel * Symbol.PipValue);
                drawChartLine(name + i, price, Color.Green, 2);
            }
        }

        public override void Calculate(int index)
        {
        }

    }
}
